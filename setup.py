from setuptools import setup

setup(name='rest_helper',
      version='0.1',
      author='cristian',
      author_email='crcasis@gmail.com',
      packages=['rest_helper'],
      install_requires=[
          'docopt',
      ],
      zip_safe=False) 